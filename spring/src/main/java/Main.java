import Domain.Participant;
import Domain.Race;
import Domain.Team;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;


public class Main {

    static public void main(String[] args) {
        ApplicationContext xmlContext=new ClassPathXmlApplicationContext("task.xml");
        Team team = xmlContext.getBean(Team.class);
        Race race = xmlContext.getBean(Race.class);
        Participant participant = xmlContext.getBean(Participant.class);
        System.out.println(team);
        System.out.println(race);
        System.out.println(participant);

        ApplicationContext javaContext = new AnnotationConfigApplicationContext(Task.class);
        Team team2 = javaContext.getBean(Team.class);
        Race race2 = javaContext.getBean(Race.class);
        Participant participant2 = javaContext.getBean(Participant.class);
        System.out.println(team2);
        System.out.println(race2);
        System.out.println(participant2);

    }
}
