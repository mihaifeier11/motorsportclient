import Domain.Participant;
import Domain.Race;
import Domain.Team;
import org.springframework.context.annotation.Bean;

public class Task {
    @Bean(name="team")
    public Team createTeam() {return new Team("Honda");}

    @Bean(name="race")
    public Race createRace() {return new Race(100, 100);}

    @Bean(name="participant")
    public Participant createParticipant() {return new Participant("John Doe", createTeam());}
}
