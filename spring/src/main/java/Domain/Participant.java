package Domain;

import java.util.Objects;

public class Participant implements Entity<Integer>{
    Integer id;
    String name;
    Team teamId;

    public Participant(Integer id, String name, Team teamId) {
        this.id = id;
        this.name = name;
        this.teamId = teamId;
    }

    public Participant(String name, Team teamId) {
        this.name = name;
        this.teamId = teamId;
    }

    public Participant(Team teamId) {
        this.teamId = teamId;
    }

    @Override
    public Integer getId() {
        return id;
    }

    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Team getTeamId() {
        return teamId;
    }

    public void setTeamId(Team teamId) {
        this.teamId = teamId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Participant that = (Participant) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(name, that.name) &&
                Objects.equals(teamId, that.teamId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, teamId);
    }

    @Override
    public String toString() {
        return "Participant{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", teamId=" + teamId +
                '}';
    }

    public String toSQL() {
        return "'" +  name + "', " + teamId;
    }
}
