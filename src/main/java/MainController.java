import Controller.Controller;
import Domain.Participant;
import Domain.Race;
import Repository.RaceRepository;
import Utils.Alerts;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Observable;
import java.util.stream.Collectors;

public class MainController {
    ObservableList<Race> races = FXCollections.observableArrayList();
    ObservableList<Integer> raceEngineCapacities = FXCollections.observableArrayList();
    ObservableList<Participant> participants = FXCollections.observableArrayList();
    Controller controller;
    Alerts alerts;
    @FXML
    TableView<Race> raceTableView;
    @FXML
    TableView<Participant> participantTableView;
    @FXML
    TableColumn<Race, Integer> engineCapacityTableColumn, nrOfParticipantsTableColumn;
    @FXML
    TableColumn<Participant, String> participantNameTableColumn;
    @FXML
    TableColumn<Participant, Integer> participantEngineCapacityTableColumn;
    @FXML
    TextField teamSearchTextField, registerParticipantNameTextField,
            registerParticipantTeamTextField;
    @FXML
    ComboBox<Integer> raceEngineCapacityComboBox, participantEngineCapacityComboBox;
    @FXML
    Button raceSearchButton, participantSearchButton, registerButton, logoutButton;


    public void initialize() {
        alerts = new Alerts();
        setColumns();
        try {
            controller = new Controller();
        } catch (ClassNotFoundException e) {
            alerts.showError(e.getMessage());
        }
        try {
            setRaceComboBox();
            raceButton();
            setInitialTableView();
            participantSetTable();
            participantSearch();
            register();
        } catch (IllegalArgumentException e) {
            alerts.showError(e.getMessage());
        }

        logoutButton.setOnAction(event -> {
            Stage primaryStage = new Stage();
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("loginPanel.fxml"));
            Parent root = null;
            try {
                root = loader.load();
            } catch (IOException e1) {
                e1.printStackTrace();
            }

            primaryStage.setScene(new Scene(root));
            primaryStage.setTitle("");
            primaryStage.show();

            Stage stage = (Stage) logoutButton.getScene().getWindow();
            stage.close();
        });

    }

    private void register() {
        try {
            raceEngineCapacities.addAll((ArrayList<Integer>) controller.getAllRaceEngineCapacities());
        } catch (SQLException e) {
            alerts.showError(e.getMessage());
        }

        participantEngineCapacityComboBox.setItems(raceEngineCapacities);
        participantEngineCapacityComboBox.getSelectionModel().selectFirst();

        registerButton.setOnAction(event -> {
            String name = registerParticipantNameTextField.getText();
            Integer engineCapacity = participantEngineCapacityComboBox.getValue();
            String teamName = registerParticipantTeamTextField.getText();
            try {
                controller.registerParticipant(name, engineCapacity, teamName);
                alerts.showInfo("Participant adaugat cu succes!");
                registerParticipantNameTextField.clear();
                registerParticipantTeamTextField.clear();
                setInitialTableView();
            } catch (Exception e) {
                e.printStackTrace();
                alerts.showError(e.getMessage());
            }

        });
    }

    private void participantSetTable() {
        participantNameTableColumn.setCellValueFactory(new PropertyValueFactory<>("name"));
        participantEngineCapacityTableColumn.setCellValueFactory(new PropertyValueFactory<>("engineCapacity"));
    }

    private void participantSearch() {
        participantSearchButton.setOnAction(event -> {
            participants.clear();

            String teamName = teamSearchTextField.getText();
            try {
                List<Participant> participantList = (List<Participant>) controller.getAllParticipantsInTeam(teamName);
                if (participantList == null) {
                    alerts.showError("Nu exista niciun participant cu acest nume.");
                } else {
                    participants.addAll(participantList);
                    participantTableView.setItems(participants);
                }
            } catch (Exception e) {
                alerts.showError(e.getMessage());
            }

            participantTableView.setItems(participants);
        });
    }

    private void raceButton() {
        raceSearchButton.setOnAction(event -> {
            races.clear();
            try {
                races.addAll(controller.getRacesWithEngineCapacity(raceEngineCapacityComboBox.getValue()));
                raceTableView.setItems(races);
            } catch (SQLException e) {
                alerts.showError(e.getMessage());
            }
        });
    }

    private void setRaceComboBox() {
        try {
            raceEngineCapacities.addAll((ArrayList<Integer>) controller.getAllRaceEngineCapacities());
        } catch (SQLException e) {
            alerts.showError(e.getMessage());
        }

        raceEngineCapacityComboBox.setItems(raceEngineCapacities);
        raceEngineCapacityComboBox.getSelectionModel().selectFirst();
    }

    private void setInitialTableView() {
        try {
            ArrayList<Race> raceList = (ArrayList<Race>) controller.getRacesWithEngineCapacity(raceEngineCapacityComboBox.getValue());
            races.clear();

            races.addAll(raceList);

            raceTableView.setItems(races);

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void setColumns() {
        engineCapacityTableColumn.setCellValueFactory(new PropertyValueFactory<>("engineCapacity"));
        nrOfParticipantsTableColumn.setCellValueFactory(new PropertyValueFactory<>("nrOfParticipants"));
    }

    public MainController() {

    }

    public void execute() {

    }
}
