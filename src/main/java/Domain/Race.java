package Domain;

import java.util.Objects;

public class Race implements Entity<Integer>{
    Integer id;
    Integer engineCapacity;
    Integer nrOfParticipants;

    public Race(Integer engineCapacity, Integer nrOfParticipants) {
        this.id = null;
        this.engineCapacity = engineCapacity;
        this.nrOfParticipants = nrOfParticipants;
    }

    public Race(Integer id, Integer engineCapacity, Integer nrOfParticipants) {
        this.id = id;
        this.engineCapacity = engineCapacity;
        this.nrOfParticipants = nrOfParticipants;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getEngineCapacity() {
        return engineCapacity;
    }

    public void setEngineCapacity(Integer engineCapacity) {
        this.engineCapacity = engineCapacity;
    }

    public Integer getNrOfParticipants() {
        return nrOfParticipants;
    }

    public void setNrOfParticipants(Integer nrOfParticipants) {
        this.nrOfParticipants = nrOfParticipants;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Race race = (Race) o;
        return Objects.equals(id, race.id) &&
                Objects.equals(engineCapacity, race.engineCapacity) &&
                Objects.equals(nrOfParticipants, race.nrOfParticipants);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, engineCapacity, nrOfParticipants);
    }

    @Override
    public String toString() {
        return "Race{" +
                "id=" + id +
                ", engineCapacity=" + engineCapacity +
                ", nrOfParticipants=" + nrOfParticipants +
                '}';
    }

    public String toSQL() {
        return engineCapacity + ", " + nrOfParticipants;
    }
}
