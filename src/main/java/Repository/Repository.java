package Repository;

import java.sql.SQLException;

public interface Repository<ID, E> {

    void createConnection() throws SQLException;

    void endConnection() throws SQLException;

    E findOne(ID id) throws ClassNotFoundException, SQLException;

    Iterable<E> findAll() throws SQLException, ClassNotFoundException;

    E save(E entity) throws SQLException, ClassNotFoundException;

    E delete(ID id) throws SQLException, ClassNotFoundException;

    E update(E entity) throws SQLException;
}

