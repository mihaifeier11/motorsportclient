import Controller.Controller;
import Utils.Alerts;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.stage.Stage;

import java.io.IOException;
import java.sql.SQLException;

public class LoginController {
    Controller controller;
    Alerts alerts;
    Stage primaryStage;
    Scene scene;

    @FXML
    Button loginButton;

    @FXML
    TextField usernameField;

    @FXML
    PasswordField passwordField;

    public void initialize() {
        execute();
    }

    public LoginController() {
        alerts = new Alerts();
        try {
            controller = new Controller();
            alerts = new Alerts();
        } catch (ClassNotFoundException e) {
            alerts.showError(e.getMessage());
        }


    }

    public void execute() {
        loginButton.setOnAction(e -> {
            login();
        });

    }

    private void login() {
        String username = usernameField.getText();
        String password = passwordField.getText();
        try {
            if (controller.login(username, password)) {
                primaryStage = new Stage();
                FXMLLoader loader = new FXMLLoader();
                loader.setLocation(getClass().getResource("mainPanel.fxml"));
                Parent root = null;
                try {
                    root = loader.load();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }

                primaryStage.setScene(new Scene(root));
                primaryStage.setTitle("");
                primaryStage.show();

                Stage stage = (Stage) loginButton.getScene().getWindow();
                stage.close();
            } else {
                alerts.showError("Parola incorecta. Va rugam sa reincercati.");
            }
        } catch (SQLException e1) {
            alerts.showError(e1.getMessage());
        }
    }
}
