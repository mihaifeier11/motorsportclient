package Validator;

import Domain.Race;
import Domain.Team;
import Repository.ParticipantRepository;
import Repository.RaceRepository;
import Repository.TeamRepository;
import Repository.UserRepository;

import java.sql.SQLException;
import java.util.List;
import java.util.regex.Pattern;

public class Validator {
    TeamRepository teamRepository;
    UserRepository userRepository;
    RaceRepository raceRepository;
    ParticipantRepository participantRepository;

    public Validator() throws ClassNotFoundException {
        teamRepository = new TeamRepository();
        userRepository = new UserRepository();
        raceRepository = new RaceRepository();
        participantRepository = new ParticipantRepository();
    }

    public void validateUsername(String username) {
        boolean b = Pattern.matches("^[a-zA-Z0-9._-]{3,}$", username);
        if (!b) {
            throw new IllegalArgumentException("Username-ul poate fi format doar din cifre, litere, punct, bara jos si minus.");
        }
    }

    public void validateTeamName(String teamName) throws SQLException, ClassNotFoundException {
        boolean b = ((List<Team>)teamRepository.findAll()).stream()
                .anyMatch(team -> team.getName().equals(teamName));
        if (!b) {
            throw new IllegalArgumentException("Echipa nu exista.");
        }
    }
    public void validateEngineCapacity(Integer engineCapacity) throws SQLException {
        boolean b = ((List<Race>)raceRepository.findAll()).stream()
                .anyMatch(race -> race.getEngineCapacity().equals(engineCapacity));
        if (!b) {
            throw new IllegalArgumentException("Nu exista nicio cursa cu aceasta capacitate.");
        }
    }

    public void validateName(String name) {
        boolean b = Pattern.matches("^[a-zA-Z -]{3,}$", name);
        if (!b) {
            throw new IllegalArgumentException("Numele poate fi format doar din litere, minus si spatii.");
        }
    }
}
