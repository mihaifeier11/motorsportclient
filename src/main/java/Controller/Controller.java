package Controller;

import Domain.Participant;
import Domain.Race;
import Domain.Team;
import Repository.ParticipantRepository;
import Repository.RaceRepository;
import Repository.TeamRepository;
import Repository.UserRepository;
import Validator.Validator;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class Controller {
    UserRepository userRepository;
    ParticipantRepository participantRepository;
    TeamRepository teamRepository;
    RaceRepository raceRepository;
    Validator validator;

    public Controller() throws ClassNotFoundException {
        userRepository = new UserRepository();
        participantRepository = new ParticipantRepository();
        teamRepository = new TeamRepository();
        raceRepository = new RaceRepository();
        validator = new Validator();
    }

    public boolean login(String username, String password) throws SQLException {
        validator.validateUsername(username);
        return userRepository.login(username, password);
    }

    public Iterable<Participant> getAllParticipants() throws SQLException, ClassNotFoundException {
        return participantRepository.findAll();
    }

    public Iterable<Participant> searchByTeam(String teamName) throws SQLException, ClassNotFoundException {
        List<Team> idTeamList = ((List<Team>) teamRepository.findAll()).stream().
                filter(team -> team.getName().equals(teamName)).collect(Collectors.toList());

        Integer idTeam = idTeamList.get(0).getId();
        return ((List<Participant>) participantRepository.findAll()).stream().
                filter(participant -> participant.getTeamId().equals(idTeam)).collect(Collectors.toList());
    }

    public Iterable<Race> getAllRaces() throws SQLException {
        return raceRepository.findAll();
    }

    public Iterable<Integer> getAllRaceEngineCapacities() throws SQLException {
        ArrayList<Integer> list = new ArrayList<>();
        raceRepository.findAll().forEach(race -> {
            if (!list.contains(race.getEngineCapacity())) {
                list.add(race.getEngineCapacity());
            }
        });

        return list;
    }

    public Iterable<Participant> getAllParticipantsInTeam(String teamName) throws SQLException, ClassNotFoundException {
        validator.validateTeamName(teamName);
        List<Team> idTeamList = ((List<Team>) teamRepository.findAll()).stream()
                .filter(team -> team.getName().equals(teamName)).collect(Collectors.toList());
        Integer idTeam = idTeamList.get(0).getId();

        List<Participant> participants = ((List<Participant>) participantRepository.findAll()).stream()
                .filter(participant -> participant.getTeamId().equals(idTeam)).collect(Collectors.toList());

        if (participants.size() == 0) {
            return null;
        }

        return participants;
    }

    public List<Race> getRacesWithEngineCapacity(Integer engineCapacity) throws SQLException {
        validator.validateEngineCapacity(engineCapacity);
        List<Race> races = ((List<Race>) raceRepository.findAll()).stream().filter(race -> race.getEngineCapacity().equals(engineCapacity)).collect(Collectors.toList());

        return races;
    }

    public Participant registerParticipant(String name, Integer engineCapacity, String teamName) throws SQLException, ClassNotFoundException {
        if (!teamName.equals("")) {
            System.out.println(teamName);
            validator.validateTeamName(teamName);
        }
        validator.validateName(name);
        validator.validateEngineCapacity(engineCapacity);
        incrementRaceWithEngineCapacity(engineCapacity);
        if (teamName.equals("")) {

            return participantRepository.save(new Participant(name, engineCapacity, null));
        }

        List<Team> idTeamList = ((List<Team>) teamRepository.findAll()).stream().filter(team -> team.getName().equals(teamName)).collect(Collectors.toList());
        if (idTeamList.size() == 0) {
            teamRepository.save(new Team(teamName));
            idTeamList = ((List<Team>) teamRepository.findAll()).stream().filter(team -> team.getName().equals(teamName)).collect(Collectors.toList());
            Integer idTeam = idTeamList.get(0).getId();
            return participantRepository.save(new Participant(name, engineCapacity, idTeam));
        }

        Integer idTeam = idTeamList.get(0).getId();
        return participantRepository.save(new Participant(name, engineCapacity, idTeam));
    }

    public Team addTeam(String teamName) throws SQLException, ClassNotFoundException {
        return teamRepository.save(new Team(teamName));
    }

    public void incrementRaceWithEngineCapacity(Integer engineCapacity) throws SQLException {
        validator.validateEngineCapacity(engineCapacity);
        List<Race> races = ((List<Race>) raceRepository.findAll()).stream()
                .filter(race -> race.getEngineCapacity().equals(engineCapacity)).collect(Collectors.toList());

        Race race = races.get(0);
        race.setNrOfParticipants(race.getNrOfParticipants() + 1);

        raceRepository.update(race);
    }
}
