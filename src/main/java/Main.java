import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("loginPanel.fxml"));
        primaryStage.setTitle("Login");
        primaryStage.setScene(new Scene(root));
        primaryStage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }
}

//
//import Domain.Participant;
//import Domain.Race;
//import Domain.Team;
//import Domain.User;
//import Repository.ParticipantRepository;
//import Repository.RaceRepository;
//import Repository.TeamRepository;
//import Repository.UserRepository;
//
//import java.sql.SQLException;
//
//public class Main {
//
//    static public void main(String[] args) {
//        try {
////            //Races
////            Race race = new Race(110, 10);
////            RaceRepository raceRepository = new RaceRepository();
////
////            raceRepository.save(race);
////            System.out.println(raceRepository.findAll());
////
////            System.out.println("--------------");
////
////
////            System.out.println(raceRepository.findAll());
////            System.out.println(raceRepository.delete(1));
////
////            System.out.println("--------------");
////
////
////
////            System.out.println(raceRepository.findOne(2));
////
////
////
////            Race race2 = new Race(13,11110, 1110);
////
////            raceRepository.update(race2);
////
////            System.out.println(raceRepository.findAll());
////
////            // Team
////            Team team = new Team("Honda");
////            TeamRepository teamRepository = new TeamRepository();
////
////            teamRepository.save(team);
////            System.out.println(teamRepository.findAll());
////
////            System.out.println("--------------");
////
////
////            System.out.println(teamRepository.findAll());
////            System.out.println(teamRepository.delete(2));
////
////            System.out.println("--------------");
////
////
////
////            System.out.println(teamRepository.findOne(1));
////
////
////
////            Team team2 = new Team(1, "Honda2");
////
////            teamRepository.update(team2);
////
////            System.out.println(teamRepository.findAll());
//
//            // User
////            User user = new User("nume", "parola");
////            UserRepository userRepository = new UserRepository();
////            System.out.println(userRepository.login("user2", "parola2"));
////
//////            userRepository.save(user);
////            System.out.println(userRepository.findAll());
////
////            System.out.println("--------------");
////
////
////            System.out.println(userRepository.findAll());
////            System.out.println(userRepository.delete(2));
////
////            System.out.println("--------------");
////
////
////
////            System.out.println(userRepository.findOne(1));
////
////
////
////            User user2 = new User(1,"user2", "parola2");
////
////            userRepository.update(user2);
////
////            System.out.println(userRepository.findAll());
//
//
////             Participant
//            Participant participant = new Participant("nume", 100, 1);
//            ParticipantRepository participantRepository = new ParticipantRepository();
//
//            participantRepository.save(participant);
//            System.out.println(participantRepository.findAll());
//
//            System.out.println("--------------");
//
//
//            System.out.println(participantRepository.findAll());
//            System.out.println(participantRepository.delete(2));
//
//            System.out.println("--------------");
//
//
//
//            System.out.println(participantRepository.findOne(1));
//
//
//
//            Participant participant2 = new Participant(1,"user2", 100, 2);
//
//            participantRepository.update(participant2);
//
//            System.out.println(participantRepository.findAll());
//
//
//        } catch (ClassNotFoundException e) {
//            e.printStackTrace();
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
//
//
//    }
//}
//

